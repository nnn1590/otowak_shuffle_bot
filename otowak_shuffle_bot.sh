#!/usr/bin/env bash
# SPDX-License-Identifier: CC0-1.0
set -e

declare BASE_DIR
BASE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]:-${0}}")"; pwd)"
readonly BASE_DIR

. "${BASE_DIR}/keys"

declare -ar WORDS=(
	ティーダの
	チンポ
	気持ち
	よすぎだろ！
)

declare TEXT
TEXT="$(shuf -e "${WORDS[@]}")"
readonly TEXT

curl -sSH "Authorization: Bearer ${ACCESS_TOKEN}" "${REMOTE}/api/v1/statuses" \
	-d "status=${TEXT}" \
	-d "visibility=unlisted"
